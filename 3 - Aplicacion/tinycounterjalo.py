## TinyCounter v1.1

import cv2
from numpy import argmax
import time
from datetime import date

from ObjetoDetectable import ObjetoDetectable
from CustomLog import CustomLog
from Engine.DatabaseService import DatabaseService
import Entorno

import concurrent.futures
import threading
import logging
import os
import imutils
import json

# Imports especificos por cliente
from Especifico.ContadorLatas import AccionesKeyboard
from Especifico.ContadorLatas import Constantes
import Especifico.ContadorLatas.Reportes

## Método que inicializa la lista de objetos detectables a partir
## del archivo objetosDetectables.json
## Autor: Jorge Fernández
## Fecha: 19/07/2020
def iniciaObjetosDetectables():
    # Leer objetos detectables de archivo
    filename = Constantes.OBJETOS_DETECTABLES_FILE

    if (not os.path.isfile(filename)):
        return None
    
    objetosDetectables = []

    with open(filename,"r") as f:
        fileContents = f.read()

    jsonContents = json.loads(fileContents)

    for objetoDetectable in jsonContents:
        objetosDetectables.append(ObjetoDetectable(objetoDetectable["classId"], objetoDetectable["descripcion"], objetoDetectable["mapaAtributos"]))

    return objetosDetectables

## Método que verificar si un objeto detectable se encuentra
## dentro del rango para entrada/salida
## Autor: Jorge Fernández
## Fecha: 19/07/2020
def isObjetoDetectableDentro(x, y):
    return (x >= Entorno.ENT and x <= Entorno.SAL)

## Método que actualiza una lista de objetos detectables en base
## a los parámetros dados
## REGLA DE NEGOCIO
## Autor: Jorge Fernández
## Fecha: 19/07/2020
def actualizaObjetosDetectables(objetosDetectables, box, confidence, classId):
    for i in range(len(objetosDetectables)):
        objetoDetectable = objetosDetectables[i]

        if objetoDetectable.getClassId() == classId:
            # Definir box y confidencia
            objetoDetectable.setConfidence(confidence)
            objetoDetectable.setBox(box)

            # Verificar entrada/salida
            estadoEnFrame = isObjetoDetectableDentro(box[0], box[1])

            # Procesar mapa de atributos
            mapaAtributos = objetoDetectable.getMapaAtributos()

            # Verificar consistencia de mapa de atributos
            if not ("esContable" in mapaAtributos and "detecciones" in mapaAtributos):
                print("[ERROR] Los atributos: esContable y detecciones no existen en el mapa del objeto detectable.")
                return objetosDetectables

            if estadoEnFrame or not Constantes.USE_ENT_SAL:
                if not mapaAtributos["esContable"] or not Constantes.USE_ENT_SAL:
                    mapaAtributos["esContable"] = True

                    # Aumentar contador de detecciones
                    mapaAtributos["detecciones"] += 1

                    if Constantes.CONSOLE_MODE:
                        print("Detección de objeto: " + objetoDetectable.getDescripcion() + ": " + str(mapaAtributos["detecciones"]))
            else:
                mapaAtributos["esContable"] = False

            objetoDetectable.setMapaAtributos(mapaAtributos)

            objetosDetectables[i] = objetoDetectable

            return objetosDetectables

## Método que procesa y devuelve los indices de los objetos detectados
## Autor: Jorge Fernández
## Fecha: 19/07/2020
def procesaIndices(objetosDetectables):
    boxes = []
    confidences = []

    for objetoDetectable in objetosDetectables:
        box = objetoDetectable.getBox()
        confidence = objetoDetectable.getConfidence()

        if len(box) > 0:
            boxes.append(box)
            confidences.append(confidence)

    return cv2.dnn.NMSBoxes(boxes,confidences,0.4,0.6)

def limpiaDeteccionObjetosDetectables(objetosDetectables):
    for i in range(len(objetosDetectables)):
        objetosDetectables[i].limpiaDeteccion()
    return objetosDetectables

## Thread que procesa los objetos detectables contra el resultado
## del forward de la red.
## Autor: Jorge Fernández
## Fecha: 13/07/2020
def procesarOutsThread(outs, objetosDetectables):
    class_ids=[]
    confidences=[]
    boxes=[]
    seProcesa = False

    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = argmax(scores)
            confidence = scores[class_id]
            if confidence > Constantes.MIN_CONFIDENCE:
                #object detected
                center_x= int(detection[0]*width)
                center_y= int(detection[1]*height)
                w = int(detection[2]*width)
                h = int(detection[3]*height)
                
                #rectangle coordinates
                x=int(center_x - w/2)
                y=int(center_y - h/2)

                # Procesa objetos detectables
                objetosDetectables = actualizaObjetosDetectables(objetosDetectables, [x,y,w,h], float(confidence), class_id)

    return objetosDetectables

## Método que ajusta el X de la entrada y salida de acuerdo a un porcentaje
## Autor: Jorge Fernández
## Fecha: 08/08/2020
def ajustaEntradaSalida(porcentaje, width, height):
    centro = width / 2
    padding = (width * porcentaje) / 2
    Entorno.ENT = round(centro - padding)
    Entorno.SAL = round(centro + padding)

# Iniciar custom log
customLog = CustomLog(Constantes.LOG_PREFIX)
customLog.log("**** TinyCounter v" + Entorno.VERSION + " iniciado ****")

if not Constantes.CONSOLE_MODE:
    customLog.log("**** Modo video ****")
else:
    customLog.log("**** Modo consola ****")
    print ("Presione Ctrl+C para salir")
    time.sleep(2.5)

customLog.log("Leyendo Red neuronal profunda...")

logging.basicConfig( level=logging.DEBUG,
    format='[%(levelname)s] - %(threadName)-10s : %(message)s')

#Load YOLO
net = cv2.dnn.readNet("tinycounter.weights","tiny_countercfg.cfg") # Original yolov3
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_OPENCL_FP16)

layer_names = net.getLayerNames()
outputlayers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
#loading image
customLog.log("Iniciando dispositivo para reproducción de video...")
cap=cv2.VideoCapture(0) ####################cambio de camara##################

if cap.isOpened == False:
    customLog.log("No fué posible inicializar el dispositivo de reproducción", 2)
    cap.release()
    cv2.destroyAllWindows()

font = cv2.FONT_HERSHEY_PLAIN
starting_time= time.time()
frame_id = 0

# Inicializar objetos detectables
customLog.log("Iniciando objetos detectables...")
objetosDetectables = iniciaObjetosDetectables()

if objetosDetectables == None:
    customLog.log("No se ha encontrado el archivo: objetosDetectables.json", 2)
    exit()

# Iniciar thread pool
customLog.log("Iniciando Pool de hilos...")
futures = []
executor = concurrent.futures.ThreadPoolExecutor(max_workers=Constantes.MAX_THREADS, thread_name_prefix='ServicioProcesador')

# Iniciar servicio de base de datos
customLog.log("Iniciando servicio de base de datos...")
databaseService = DatabaseService(customLog)

if Constantes.USE_DB:
    if databaseService.creaConexionDB(Constantes.DB_HOST, Constantes.DB_USER, Constantes.DB_PASSWORD, Constantes.DB_NAME, Constantes.DB_PROVIDER) == None:
        customLog.log("El servicio de base de datos no ha podido ser iniciado", 1)

#Thread para guardar reporte recurrente
if Constantes.AUTO_SAVE:
    executor.submit(Especifico.ContadorLatas.Reportes.generaReportesThread, objetosDetectables, databaseService)

# Iniciar bucle de procesamiento
primerBucle = False
customLog.log("All set...")
while True:
    isNewThread = True
    ################################################codigo yahir
    _,frame= cap.read(1)
    height,width,channels = frame.shape
    rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    rgb = imutils.resize(frame, width=750)
    blob = cv2.dnn.blobFromImage(frame,0.00392,(320,320),(0,0,0),True,crop=False) #reduce 416 to 320    

    frame_id+=1
    
    # Bloque del primer bucle
    if not primerBucle:
        primerBucle = True

        #Ajuste de entrada y salida con porcentaje
        if Constantes.USE_ENT_SAL:
            if Constantes.ENT_SAL_PORCENTAJE > 0:
                ajustaEntradaSalida(Constantes.ENT_SAL_PORCENTAJE, width, height)
            else:
                Constantes.USE_ENT_SAL = False
                customLog.log("La variable ENT_SAL_PORCENTAJE es 0, se quitará chequeo por entrada y salida.", 1)

    # Ingresar BLOB a network
    net.setInput(blob)
    outs = net.forward(outputlayers)

    # Renderizar información de atributos para objetos detectables
    if not Constantes.CONSOLE_MODE:
        actualY = 25
        for objetoDetectable in objetosDetectables:
            mapaAtributos = objetoDetectable.getMapaAtributos()

            cv2.putText(frame, objetoDetectable.getDescripcion(), (5,actualY), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,(255,255,255) )

            actualY += 25
            for atributo in mapaAtributos.items():
                cv2.putText(frame, atributo[0] + ": " + str(atributo[1]), (15,actualY), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,(0,0,255) )
                actualY += 25
            actualY += 25

        # Renderizar lineas de entrada/salida
        if Constantes.USE_ENT_SAL:
            cv2.line(frame,(Entorno.ENT,0),(Entorno.ENT,height),(0,0,255),3)
            cv2.line(frame,(Entorno.SAL,0),(Entorno.SAL,height),(0,0,255),3)
    
    # Reiniciar atributos de detección para los objetos detectables
    objetosDetectables = limpiaDeteccionObjetosDetectables(objetosDetectables)

    # Se genera thread en pool
    future = executor.submit(procesarOutsThread, outs, objetosDetectables)

    # Iterar threads terminados para obtener su resultado
    for i in range(len(futures)):
        if futures[i].done():
            objetosDetectables = futures[i].result()
            # Acomodar nuevo future en el array de futures
            futures[i] = future
            isNewThread = False
    
    if not isNewThread:
        futures.append(future)

    # Procesa y obtiene los indices normalizados para las detecciones
    indexes = procesaIndices(objetosDetectables)

    # Renderiza areas detectadas
    if not Constantes.CONSOLE_MODE:
        for i in range(len(objetosDetectables)):
            if i in indexes:
                x,y,w,h = objetosDetectables[i].getBox()
                label = objetosDetectables[i].getDescripcion()
                confidence= objetosDetectables[i].getConfidence()
                color = objetosDetectables[i].getColor()

                cv2.rectangle(frame,(x,y),(x+w,y+h),color,2)
                cv2.putText(frame,label+" "+str(round(confidence,2) * 100) + "%",(x,y+30),font,1,(255,255,255),2)


    # Mostrar tiempo de inferencia y FPS's
    elapsed_time = time.time() - starting_time
    fps=str(round((frame_id/elapsed_time), 2))

    if not Constantes.CONSOLE_MODE:
        cv2.putText(frame,"FPS: "+fps,(10,height-10),font,1,(255,255,255),1)
    else:
        if Constantes.CONSOLE_MODE_DISPLAY_FPS:
            if frame_id % 100 == 1:
                print("Bucles por segundo: " + fps)

    # Eventos para acciones con teclado
    key = cv2.waitKey(1)
    objetosDetectables = AccionesKeyboard.teclaReinicioDetecciones(key, objetosDetectables)

    if AccionesKeyboard.teclaSalida(key, objetosDetectables, databaseService):
        cap.release()
        cv2.destroyAllWindows()
        break

    # Mostrar frame
    if not Constantes.CONSOLE_MODE:
        cv2.imshow("TinyCounter v" + Entorno.VERSION, frame)