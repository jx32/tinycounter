import xlwt
from xlwt import Workbook
from datetime import date
import time

from CustomLog import CustomLog
from . import Constantes

## Thread que guarda reportes cada cierto tiempo
## Autor: Jorge Fernández
## !! Problema conocido !!:
## Al momento de estar en consola y terminar el proceso este thread
## aún sigue funcionando y no termina hasta cerrar la terminal.
## Fecha: 08/08/2020
def generaReportesThread(objetosDetectables, databaseService):
    while (True):
        time.sleep(Constantes.AUTO_SAVE_SECONDS)
        generaReporteInventario(objetosDetectables, databaseService)

## Método que genera un reporte con un inventario
## Autor: Jorge Fernández
## Fecha: 13/07/2020
def generaReporteInventario(objetosDetectables, databaseService, generaReporteDB=False):
    # Log
    customLog = CustomLog(Constantes.LOG_PREFIX)
    customLog.log("Generando reporte de inventario, Excel...")

    wb = Workbook()

    #Inicializar hoja de cálculo
    sheet1 = wb.add_sheet('Inventario ' + str(date.today()))
    sheetObjetoY = 1

    for objetoDetectable in objetosDetectables:
        #Escribir descripción de objeto detectable
        sheet1.write(sheetObjetoY, 0, objetoDetectable.getDescripcion())

        #Escribir sus atributos
        mapaAtributos = objetoDetectable.getMapaAtributos()
        sheetAtributosX = 1

        for atributo in mapaAtributos.items():
            sheet1.write(sheetObjetoY - 1, sheetAtributosX, atributo[0])
            sheet1.write(sheetObjetoY, sheetAtributosX, atributo[1])

            sheetObjetoY += 3

        #Escribir reporte
        wb.save(str(date.today()) + " " + Constantes.REPORT_PREFIX + Constantes.REPORT_EXT)
    
    # Escribir reporte en base de datos
    if Constantes.USE_DB and databaseService.existeConexion() and generaReporteDB:
        generaReporteBaseDeDatos(objetosDetectables, databaseService, customLog)

## Método que genera los registros para el inventario
## en base de datos
## Autor: Jorge Fernández
## Fecha: 08/08/2020
def generaReporteBaseDeDatos(objetosDetectables, databaseService, customLog):
    customLog.log("Generando reporte de inventario, Base de datos...")

    sql = ""

    for objetoDetectable in objetosDetectables:
        mapaAtributos = objetoDetectable.getMapaAtributos()

        if mapaAtributos["detecciones"] > 0:
            sql = "INSERT INTO inventario (PROD_IDENTIFICADOR, TOTAL) "
            sql += "VALUES ((SELECT IDENTIFICADOR FROM productos WHERE UPPER(DESCRIPCION) = UPPER('" + objetoDetectable.getDescripcion() + "')), " + str(mapaAtributos["detecciones"]) + "); "            

            databaseService.ejecutaConsulta(sql)