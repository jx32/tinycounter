#Modo de uso
CONSOLE_MODE=True
# Si CONSOLE_MODE es True, variable para mostrar los FPS en consola
CONSOLE_MODE_DISPLAY_FPS=True

#Confidencia minima
MIN_CONFIDENCE=0.75
#Hilos máximos
MAX_THREADS=5
#Utilizar lineas entrada/salida
USE_ENT_SAL=True
#Rangos de detección entrada/salida
ENT_SAL_PORCENTAJE=0.35

#Parametros para rutas de deltas de configuración
OBJETOS_DETECTABLES_FILE="objetosDetectables.json"

#Parámetros para reportes o logs
REPORT_PREFIX="Inventario de producción"
REPORT_EXT=".xls"
LOG_PREFIX="Contador_Latas_Log"
## Bug conocido con AUTO_SAVE !!!!
## Al momento de estar en consola y terminar el proceso este thread
## aún sigue funcionando y no termina hasta cerrar la terminal.
AUTO_SAVE=False
AUTO_SAVE_SECONDS=10

#Parámetros para base de datos
USE_DB=False
DB_HOST="127.0.0.1"
DB_USER="root"
DB_PASSWORD=""
DB_NAME="TINYCOUNTER"
DB_PROVIDER="mysql"