from CustomLog import CustomLog
from . import Constantes
import Especifico.ContadorLatas.Reportes

from datetime import date

## Método que detecta si la tecla para reiniciar las detecciones
## fue presionada
## Autor: Jorge Fernández
## Fecha: 13/07/2020
def teclaReinicioDetecciones(key, objetosDetectables):
    if key == ord("w"):
        # Definir detecciones totales a 0
        for i in range(len(objetosDetectables)):
            mapaAtributos = objetosDetectables[i].getMapaAtributos()
            mapaAtributos["detecciones"] = 0
            objetosDetectables[i].setMapaAtributos(mapaAtributos)

            # Log
            customLog = CustomLog(Constantes.LOG_PREFIX)
            customLog.log("Se ha reiniciado el contador de detecciones")

    return objetosDetectables

## Método que detecta si la tecla para salir
## fue presionada
## Autor: Jorge Fernández
## Fecha: 13/07/2020
def teclaSalida(key, objetosDetectables, databaseService):
    if key == 27:
        # Generar reporte
        Especifico.ContadorLatas.Reportes.generaReporteInventario(objetosDetectables, databaseService, True)

        # Log
        customLog = CustomLog(Constantes.LOG_PREFIX)
        customLog.log("Fin de ejecución (usuario).")

        return True
    return False