import time
from datetime import date

class CustomLog:

    def __init__ (self, filename=None):

        if filename == None:
            self.filename = str(date.today())
        else:
            self.filename = filename

    def log(self, text, severity=0, module=None):
        t = time.localtime()
        filename = self.filename + " " + str(date.today()) + ".txt"
        
        if module == None:
            module = ""
        else:
            module = "[" + module + "] "

        if severity == 0:
            severityMessage = "Info"
        elif severity == 1:
            severityMessage = "Warning"
        elif severity == 2:
            severityMessage = "Error"

        text = "[" + time.strftime("%H:%M:%S", t) + "][" + severityMessage + "] " + module + text

        f = open(filename, "a+")
        f.write(text + "\n")

        print(text)
