import unittest

from Especifico.ContadorLatas.AccionesKeyboard import *
from ObjetoDetectable import ObjetoDetectable

class UnitTestsAccionesKeyboard(unittest.TestCase):

    def test_teclaSalida(self):
        self.assertTrue(teclaSalida(27, [ObjetoDetectable(0, "Test", { "Test": 1 })], None))

if __name__ == '__main__':
    unittest.main()