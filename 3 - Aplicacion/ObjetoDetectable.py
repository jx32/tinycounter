from numpy import random

class ObjetoDetectable:

    def __init__ (self, class_id, descripcion, mapaAtributos, color=None):
        self.class_id = class_id
        self.descripcion = descripcion
        self.mapaAtributos = mapaAtributos
        self.confidence = 0.0
        self.box = [0, 0, 0, 0]

        if color == None:
            self.color = random.uniform(0,255,size=1)
        else:
            self.color = color

    def limpiaDeteccion(self):
        self.setConfidence(0.0)
        self.setBox([0, 0, 0, 0])

    # Getter's y setter's
    def getClassId (self):
        return self.class_id

    def setClassId (self, class_id):
        self.class_id = class_id

    def getDescripcion (self):
        return self.descripcion

    def setDescripcion (self, descripcion):
        self.descripcion = descripcion

    # Getter's y setter's para detección
    def getMapaAtributos (self):
        return self.mapaAtributos

    def setMapaAtributos (self, mapaAtributos):
        self.mapaAtributos = mapaAtributos

    def getConfidence(self):
        return self.confidence

    def setConfidence(self, confidence):
        self.confidence = confidence

    def getBox(self):
        return self.box

    def setBox(self, box):
        self.box = box

    def getColor(self):
        return self.color

    def setColor(self, color):
        self.color = color