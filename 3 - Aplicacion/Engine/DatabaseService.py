import mysql.connector
from mysql.connector import errorcode

class DatabaseService:
    
    def __init__ (self, customLog):
        self.customLog = customLog
        self.conexion = None

    ## Método que genera una conexión a la base de datos usando
    ## el driver MySQL
    ## TODO: Adaptar a diferentes tipos de proveedores como Oracle o Postgree
    ## Autor: Jorge Fernández
    ## Fecha: 08/08/2020
    def creaConexionDB(self, host, user, password, name, proveedor):
        resultado = None
        
        if proveedor == "mysql":
            try:
                resultado = mysql.connector.connect(host=host, user=user, password=password, database=name)   
            except mysql.connector.Error as err:
                if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                    self.customLog.log("Usuario o contraseña incorrectos", 2)
                elif err.errno == errorcode.ER_BAD_DB_ERROR:
                    self.customLog.log("La base de datos " + name + " no existe", 2)
                else:
                    self.customLog.log("No se ha podido conectar a la base de datos debido a: " + err, 2)
        else:
            self.customLog.log("No ha sido posible encontrar al proveedor especificado", 2)
        
        self.conexion = resultado
        return resultado

    ## Método que busca si existe una conexion activa
    ## Autor: Jorge Fernández
    ## Fecha: 08/08/2020
    def existeConexion(self):
        return (self.conexion != None)

    ## Método que ejecuta una consulta y devuelve el resultado de esta
    ## Autor: Jorge Fernández
    ## Fecha: 08/08/2020
    def ejecutaConsulta(self, consulta, datos=None):
        if not self.existeConexion():
            self.customLog.log("Antes de ejecutar una consulta, genere una conexión a base de datos", 1)
            return None
        
        try:
            cursor = self.conexion.cursor()
            
            if datos == None:
                cursor.execute(consulta)
            else:
                cursor.execute(consulta, datos)

            self.conexion.commit()
            self.customLog.log("Se ejecutó la consulta: " + consulta)

            return cursor
        except:
            self.customLog.log("Error al ejecutar la consulta: " + consulta, 2)
            self.conexion.rollback()

        return None